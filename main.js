// Second smallest and second largest numbers function

function getnumbers(numbers) {
numbers.sort(function(a, b) {
return a - b;
});
return numbers[1] + ", " + numbers[numbers.length - 2];
}

console.log( "Second smallest and second largest numbers are: " + getnumbers([22, 19, 92, 191, 50, 442, 90, 101]));
